function addNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 + num2;
	return false;
}

document.getElementById('add').addEventListener('click', addNumbers);

function minusNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 - num2;
	return false;
}

document.getElementById('minus').addEventListener('click', minusNumbers);

function multiplyNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 * num2;
	return false;
}

document.getElementById('multiply').addEventListener('click', multiplyNumbers);

function divideNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 / num2;
	return false;
}

document.getElementById('divide').addEventListener('click', divideNumbers);

function modulosNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 % num2;
	return false;
}

document.getElementById('modulos').addEventListener('click', modulosNumbers);

function clearNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);
	if (buttonValues == 'clear') {
    document.getElementById('result').innerHTML = "";
    return;  
}
}

document.getElementById('clear').addEventListener('click', clearNumbers);